#!/bin/sh

pkill cmpp.ismg
pkill cmpp.ismg

export CONF_LOG_PATH="./logs"
export CONF_LOG_TIME_FORMAT="2006-01-02T15:04:05.000"

# optional args --port 1234 --multicore=false
# default  args --port 9000 --multicore=true
nohup ./cmpp.ismg --port 9000 --multicore=true >panic.log 2>&1 &

sleep 3
tail -10 ./logs/cmpp.log
sleep 7
top -pid "$(cat cmpp.pid)"
