package main

import (
	"math/rand"
	"time"

	"github.com/hrygo/yaml_config"
	"github.com/ryhgo/log"
	"go.uber.org/zap"

	"github.com/hrygo/gosms/codec/cmpp"
	"github.com/hrygo/gosms/comm"
	"github.com/hrygo/gosms/comm/snowflake"
)

func main() {
	rand.Seed(time.Now().Unix()) // 随机种子
	logInit()

	cmpp.Conf = yaml_config.CreateYamlFactory("config", "cmpp", "gosms")
	cmpp.Conf.ConfigFileChangeListen()

	dc := cmpp.Conf.GetInt("data-center-id")
	wk := cmpp.Conf.GetInt("worker-id")
	cmpp.Seq32 = comm.NewCycleSequence(int32(dc), int32(wk))
	cmpp.Seq64 = snowflake.NewSnowflake(int64(dc), int64(wk))
	cmpp.ReportSeq = comm.NewCycleSequence(int32(dc), int32(wk))
	StartServer()
}

func logInit() {
	var tops = []log.TeeOption{
		{
			Filename: log.BasePath() + "cmpp.log",
			Ropt: log.RotateOptions{
				MaxSize:    100,
				MaxAge:     30,
				MaxBackups: 100,
				Compress:   true,
				Format:     "console",
			},
			Lef: func(lvl log.Level) bool {
				return lvl <= log.FatalLevel && lvl > log.DebugLevel
			},
		},
		{
			Filename: log.BasePath() + "error.log",
			Ropt: log.RotateOptions{
				MaxSize:    10,
				MaxAge:     7,
				MaxBackups: 10,
				Compress:   false,
				Format:     "console",
			},
			Lef: func(lvl log.Level) bool {
				return lvl > log.InfoLevel
			},
		},
	}

	logger := log.NewTeeWithRotate(
		tops,
		zap.AddStacktrace(zap.ErrorLevel),
		log.WithCaller(true),
	)
	log.ResetDefault(logger)
}
