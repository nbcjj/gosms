package main

import (
	"math/rand"
	"time"

	"github.com/hrygo/yaml_config"
	"github.com/ryhgo/log"
	"go.uber.org/zap"

	"github.com/hrygo/gosms/codec/smgp"
	"github.com/hrygo/gosms/comm"
)

func main() {
	rand.Seed(time.Now().Unix()) // 随机种子
	logInit()

	smgp.Conf = yaml_config.CreateYamlFactory("config", "smgp", "gosms")
	smgp.Conf.ConfigFileChangeListen()

	dc := smgp.Conf.GetInt("data-center-id")
	wk := smgp.Conf.GetInt("worker-id")
	smgwId := smgp.Conf.GetString("smgw-id")
	smgp.Seq32 = comm.NewCycleSequence(int32(dc), int32(wk))
	smgp.Seq80 = comm.NewBcdSequence(smgwId)
	StartServer()
}

func logInit() {
	var tops = []log.TeeOption{
		{
			Filename: log.BasePath() + "smgp.log",
			Ropt: log.RotateOptions{
				MaxSize:    100,
				MaxAge:     30,
				MaxBackups: 100,
				Compress:   true,
				Format:     log.ConsoleFormat,
			},
			Lef: func(lvl log.Level) bool {
				return lvl <= log.FatalLevel && lvl > log.DebugLevel
			},
		},
		{
			Filename: log.BasePath() + "error.log",
			Ropt: log.RotateOptions{
				MaxSize:    10,
				MaxAge:     7,
				MaxBackups: 10,
				Compress:   false,
				Format:     log.JsonFormat,
			},
			Lef: func(lvl log.Level) bool {
				return lvl > log.InfoLevel
			},
		},
	}

	logger := log.NewTeeWithRotate(
		tops,
		zap.AddStacktrace(zap.ErrorLevel),
		log.WithCaller(true),
	)
	log.ResetDefault(logger)
}
